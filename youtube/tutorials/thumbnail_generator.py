#!/usr/bin/env python3

import sys
import argparse
import os
from PIL import Image, ImageDraw, ImageFont

# Creates and saves an image with the text and number passed as parameter
def create_image_file(output_path, number, title, language = "en"):
  # Load template.
  dir_path = os.path.dirname(os.path.realpath(__file__))
  with Image.open(os.path.abspath(dir_path + "/solarus_tutorial_thumbnail.template.png"), mode="r") as template:
    image_size = template.size
    draw = ImageDraw.Draw(template)

    # Draw series title
    languages = {
      "en": "Solarus 1.6 Tutorial",
      "fr": "Tutoriel Solarus 1.6",
    }

    series_title_font = ImageFont.truetype("UbuntuTitling-Bold", 180)
    series_title_text = languages[language]
    series_title_size = series_title_font.getsize(series_title_text)
    series_title_pos_x = (image_size[0] - series_title_size[0]) / 2
    series_title_pos_y = 430
    draw.text((series_title_pos_x, series_title_pos_y), series_title_text, (255,188,0), font=series_title_font)

    # Draw tutorial number.
    number_font = ImageFont.truetype("UbuntuTitling-Bold", 200)
    number_text = "#%d" % number
    number_size = number_font.getsize(number_text)
    number_pos_x = (image_size[0] - number_size[0]) / 2
    number_pos_y = 670
    draw.text((number_pos_x, number_pos_y), number_text, (255,255,255), font=number_font)

    # Tutorial title.
    title = title.strip()

    # Languages specificities
    if language == "fr":
      # If French, quick and dirty way of handling non-breaking spaces for most common usages.
      title = title.replace(" !", chr(160) + "!")
      title = title.replace(" ?", chr(160) + "?")
      title = title.capitalize() # First word's first letter is always uppercase
    elif language == "en":
        title = title.title() # All first letters are uppercase

    title_font = ImageFont.truetype("UbuntuTitling-Bold", 200)
    title_words = title.split(chr(32))
    line_width = 0
    current_line = 0
    lines = [[""]]
    available_width = image_size[0] - 180 * 2 # right and left margins
    for word in title_words:
      # Quick and dirty word wrap for lines.
      word_width = title_font.getsize(word)[0]
      if line_width + word_width <= available_width:
        line_width += word_width
        lines[current_line].append(word)
      else:
        current_line += 1
        line_width = word_width
        lines.append([word])

    text_line_pos_y = 700
    for line in lines:
      text_line = str.join(" ", line).strip()
      text_line_size = title_font.getsize(text_line)
      text_line_pos_x = (image_size[0] - text_line_size[0]) / 2
      text_line_pos_y += text_line_size[1] * 1.1
      draw.text((text_line_pos_x, text_line_pos_y), text_line, (255,255,255), font=title_font)

    # Save image.
    if not os.path.exists(output_path):
      os.mkdir(output_path)
    output_filepath = os.path.abspath(output_path +
      "/solarus_tutorial_thumbnail_%(number)d_%(language)s.jpeg" % {"number":number, "language":language})
    template.save(output_filepath, 'jpeg', quality=90, optimize=True, progressive=True)
    print("Created image:", output_filepath)

# Main
if __name__ == '__main__':
  # Create parameter parser.
  parser = argparse.ArgumentParser(description="Script to generate a JPEG image for a Youtube Solarus tutorial thumbnail.")
  parser.add_argument("-number", type=int, help="Tutorial number")
  parser.add_argument("-title", type=str, help="Tutorial title (keep it short so it fits on 2 lines max)")
  parser.add_argument("-lang", type=str, help="Language (\"en or \"fr)")
  parser.add_argument("-output", type=str, help="Output folder path")

  # Parse parameters.
  args = parser.parse_args()

  if args.output is None:
    args.output = os.path.abspath("./tmp")

  # Create image file.
  create_image_file(args.output, args.number, args.title, args.lang)
